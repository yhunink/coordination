
# PrivateTracer
From global intelligent lockdown to a global intelligent opening. We must all do this together.

The goal is to enable citizens to track who they have been in contact with, and then to receive notifications when contacts are later diagnosed as infected. This breaks the chain of transmission - thus empowering them to take appropriate actions. And to do this while preserving privacy.

## Context
Tracking of connections between citizens in the context of reducing the spread of infections is a critical tool as societies adapt to the impact of Covid 19. This applies both during the suppression phase where large parts of society are shut down to reduce social connections which while reducing interactions does not fully eliminate them, but even more so as society ramps back up after it is clear that the peak of virus infections has been sufficiently flattened. Information is key - both at the micro scale to understand who has interacted with who, as well as at the macro scale whereby trends and social patterns as well as hot spots can be identified. The data is available, or readily obtainable with additional measures - but as has been repeatedly shown from history measures implemented in a crisis often remain long term. In responding to the crisis many of the options are framed as balancing the economic impact with the health impact for society - but there critically needs to be a third angle to the discussion: privacy. The challenge for us is how to enable the citizens in our society to effectively respond to the crisis, supporting the government and stimulate citizen empowerment both in terms of actions and information while also retaining protection on the data that is shared and how it is used.

*Our Dream:* Empower the citizens in their dealing with the spread of the virus, re-invigorating our democratic societies. This would be a victory not only against the coronavirus, but against all future epidemics and crises that might assail humankind in the 21st century.

*Our Nightmare*: the measures we put in place erode further privacy in society, ultimately leading to totalitarian surveillance type of society. Citizens lose their freedom and privacy.

### Infection Risk via Contact Tracing
The ability to know who an infected person has interacted with is key to being able to minimise the further spread of the virus. The term widely used for this is Contact Tracing.

The risk of infection from a particular interaction is an area of active research. The key aspects for determining the infection risk are:
* **Proximity**: How close was the user to another user, and for how long
* **Environment**: Where did the interaction take place? It is established that interactions indoor have a higher risk than interactions outdoor
There are already deployed solution that leverage Bluetooth LE for detecting locally devices in close proximity, and the duration of such proximity can also be tracked. 

However additional **calibration** is also needed to be able to take the raw Bluetooth data and turn it into information related to the distance of the other device - and to what end this can be of sufficent resolutioni. Further, this data may need to be combined with meta information about the location to derive an actual risk of infection - which ultimately is what is important. 

It is our belief that detecting the actual infection risk from an interaction is a topic that will require wide collaboration - from ensuring dominant client OS (e.g. iOS, Android) are updated to faciliate the interactions needed at the reliability needed, calibration per device pair to proximity data as well as what other data sources (e.g. local gps data that is already known to the device) are used to determine the environment where the interaction took place. 

## Key principles
The chosen approach has the following key aspects 
*  Privacy by Design: The recording of interactions, and how this is then used to prevent the spread of the virus, is done with privacy as the basis for the fundamental design. 
*  Proximity Tracking: the solution focuses on local interactions. All data captured is stored securely on peoples mobile phones and not on a central server.
*  Notifications: all matching of interactions should be done on local devices.
*  Open source: all code created from the solution would be open source, thereby helping to build a wider community to develop the approach further and make it accessible to every country.
*  Self sovereign: For the storage of interactions, this would be done in a secure repository on the device. The information that is exchanged in an interaction would leverage the DP-3T protocol. The user remains at all times in full control of their data. 

The above approach we believe, because we focus on citizen empowerment driven by technology designed with privacy and data sovereignty in mind. We do this by using a distributed identity privacy protocol and stimulating governments worldwide to strengthen their relationship with their citizens.

## Further information
Please see the following documents for additional information and how to engage with the project:
* [Technical Design](./TechnicalDesign.md) - For more information on the underlying design of PrivateTracer
* [Development Setup](./DevelopmentSetup.md) - For information on how the project is structured, top level repositories, roles, etc
* [Coordination](https://gitlab.com/PrivateTracer/coordination/-/boards): For coordination of activities around the projects. Note: the board is in need of a clean up.
* [Use cases](./usecases/Overview.md): For descriptions, interaction diagrams etc for key use cases related to privacy preserving contact tracing.
* [Security](./Security/README.md): Security related information.
* [Software Quality](./SoftwareQualityStatus.md): Software quality assessment.
* Getting Started (for developers) - to be added
