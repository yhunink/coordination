# Coding Standards
This is a general overview of the coding standards of this project. As multiple programming languages with each their own approach we will not attempt to create one standard for all of them. There will be deviations. However, we will try to adhere to certain guidelines that can be uphold for all languages.

## Basic Guidelines

* **Don't make methods too large**. A method that is too large (think 20+ line of code) is likely doing too much which makes it hard to understand, test and maintain. Split your code into smaller methods.
* **Don't make methods too complicated**. Once again, code that is hard to understand is hard to test and maintain. Complexity is easily measured by counting the amount of if-statement and loops. A good target is below 5.
* **Don't repeat yourself**. Doing stuff multiple times, means possibilities of multiple errors. You might only notice one of them, fix them and still be left with a bug. Don't do it.
* **Do comment the intent of methods/classes**. Describing what code is supposed to do (or should be used for) is more important than describing what it does.
* **Limit inline comments**. If you need to explain code inside your method, it could likely be extracted to a different method. Comments *in* your methods are an indication that the method itself is becoming too complicated.
* **Do not comment out code**. Just remove it. Commented code can soon be outdated or worse, misplaced. Version control will take care of your old code, if you really think it is important make the commit message clear so you can find it back later.
* **No unused variables, functions, imports**. If you don't need it, don't use it. It can only lead to confusion/more things to follow.
* **Use an auto-formatter with project-wide configuratoin**. Everybody likes consistency, you probably do too. An auto-formatter helps your maintain that consistency effortlessly.
* **Do use clear and consistent names for variables, functions and classes**. Clear to understand code is more important than comments (which you might forget to write). Therefore try to use names like `imageCounter` instead of `c`.
* **Be a good boyscout**. If you see things that can be done better, please do. Improve everyones lives.