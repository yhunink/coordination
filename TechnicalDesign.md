# Introduction

This is a working document intended to capture technical design decisions for the implementation. The reader is assumed to have read the project overview from README.md.

In all discussions below, the concept of privacy by design is fundamental - if any  threats are not adequately addressed then please share directly. 

The design below is still iterating - and no doubt will evolve further. 

Finally, this document focuses on one core use case - but the basic design should be expandable to cover additional use cases with incremental additions (whilst adhering to privacy principles) e.g. sharing of (anonymized) data to health authorities, proximity warnings, aggregating multiple factors in determining the risk profile of a person etc.

# Architecture
The high level conceptual architecture is shown below. 

![High level aarchitecture](Images/TechDesign-Arch.png "High level archtitecture")

The key components on the client include:
* Local Storage: On device repository for all information related to detected interactions
* DP-3T Protocol: Responsible for generating all temporary identifiers used for local interactions, and for detecting if any known local interactions match latest data shared from the server.
* Proximity Detection: Monitoring and detection of nearby devices, and then calibrates information into actionable proximity data. 
* Server communications: Responsible for managing all interactions with cloud based services

The key server side components include:
* Devices Interface: Provides the set of services that devices use to share infected interactions as well as to obtain the latest known set of infected interactions
* DP-3T Aggregator: Gathers all known temporary identifiers that are known to related to people that have tested positive for sharing with clients in an effective manner.
* Storage: for storing information related to the known set of infected interactions
* Proxy: Interface for supporting additional cloud based interactions e.g. for roaming, providing dashboards using anonymized data etc. 

# Infection Risk

There are many factors that have been identified as relevant in determining the infection risk from an interaction: 
* Proximity
* Duration
* Environment (inside / outside)
* User protection worn
And this list can no doubt be further extended.

There are also multiple sources of data that can be considered when trying to measure locally ER:
* Bluetooth: currently the focus of most attention, as it can be used to give a measure of proximity
* Local Location Data: the users already know their location, so if this is sufficiently high resolution then peer devices potentially can identify the distance to the other device
* Sonar pulses: one device emits a signal that can be detected / analyzed by the peer device
And this list can also no doubt be further extended. 

Local data is then combined to identify the risk of infection - which could also take into account the profile of the person (if the device user so chooses....). 

## Collaborative Effort
The current accuracy in being able to identify the infection risk - based largely on proximity detection via bluetooth - is being actively investigated. First results are starting to appearing from research institutes, but there is as yet not a scientific consensus that in particular Bluetooth by itself can be sufficiently calibrated into proximity that results in an acceptable level of false positives. 

It is expected the resolution & accuracy of the exposure risk will improve with wider collaboration, which by necessity has to include experts from a wide variety of disciplines such as epidemiologists, sociologists, ...

The whole topic of how to accurately detect interactions that have a high infection risk level is one that requires wide and further collaboration across society. 


## Bluetooth (current implementation)

In the current approach,  where bluetooth is used for local communication, phones advertise their identifiers through BLE advertisement. The receiving device then captures information related to characteristis of the signal of the other device.. This information then needs to be calibrated to ensure that it can be translated into actual information about the interaction e.g. how far away the other device is. It should be emphasised that this is at a coarse accuracy level in the expectation that collaboration will be put in place with additional parties that are focusing on this topic.

In parallel to the advertisement, the phones scan for similar advertisements. The interaction identifiers are attached to the advertisement as service data, recognizable by a 16-bit (2 byte) identifier: `0x1D1D`. For the bluetooth spec this is transformed to a 16-bit UUID:
```
00001D1D-0000-1000-8000-00805F9B34FB
```
This way, BLE scanners know where the data can be found and can filter any unwanted BLE signals.

Finally, there is something extra going on to circumvent the issue on iPhones where apps cannot advertise on bluetooth when the app is in the background. iOS apps are allowed to wake up through nearby iBeacons. We leverage this feature by letting apps on Android phones act as an iBeacon in addition to the regular advertisement. This advertisement is recognizable by its proximity id:
```
c8c299fb-9ccd-4322-a1e6-790099fe62c6
```
Once the iPhone wakes yup through the iBeacon, it will be able to shortly broadcast its signal. This solves the iOS problem when an android phone is near (though not when there are solely iPhones in the proximity). It is likely that the iBeacon workaround can be removed when Google and Apple release their updates regarding contact tracing.


## Automatic Detection 
It is in particular the interactions between people that do not know each other, or that may not realise they have been exposed to an infection risk that need to be tracked. 

This implies that the interaction tracking has to be automatic - if a user has to do an explicit action to register an interaction event then the chances are high that relevant interactions will simply be missed.

That said, it is possible to notify the user i.e. use the automatic detection to bring the user in the loop by asking them to confirm the interaction. This ensures that the user is engaged, and also aims to reduce the level of false positives. If the user opts in, this could potentially also help refine the infection risk calibration data. 

# Local storage

All data related to interactions is stored locally on the device.

For each interaction, a device generates a new PeerDID and uses that to then generate a unique identifier for the interaction per device (i.e. pairwise unique identifiers). These identifiers are called Interaction Identifiers (_IID_). All information related to the interaction is stored under this identifier. 

Over time each device will thus have a sequence of records of interactions that it has encountered.


![Interactions stored on device](Images/TechDesign-Interactions.png "Storage of interactions on devices")




Taking the example shown above, and as per DP-3T specification, device A will generate a new temporary identifier as will device B - all local to each device. Zooming in to the information relevant to the interaction between device A and device B:

![Detailed view of interaction as stored on device](Images/TechDesign-InteractionDetailView.png "Close up view of a pair of interaction records for a single interaction")

The EphID is generated using the daily seed.

## Tracking

For each interaction, a device stores the following

*   The remote EphID (of the peer device), representing the interaction from the perspective of the local device
*   MetaData - additional information about the interaction e.g. signal strength, duration, potentially also low resolution location data such as the city, etc

The meta data that is stored has be chosen carefully to avoid privacy leakages e.g. by not storing exact time / location data by which a user could potentially identify the other infected person. This can be mitigated by storing reduced resolution information e.g. time period / rough location from the interaction. It is an open topic about how to balance this with the expected need to be able to enable the user to opt-in for sharing anonymized interaction data for macro level analysis. 


![Populated interaction record as stored on device](Images/TechDesign-InteractionPopulated.png "Storage of populated interaction on devices")

The result is that each client device will have captured a sequence of interactions, each identified the IID generated by the remote devices it has interacted with. 

![Populated multiple interaction records as stored on devices](Images/TechDesign-Interactions2.png "Storage of populated interactions on devices")



# Notifications

It is critical that once a person is identified as being infected, or even as having a high probability of being infected, that all interactions related to that device can be marked as “infected interactions” and that devices can identify whether they have participated in such interactions.

In the case of TraceTogether, this role is fulfilled by the government. In our case the goal is to design an efficient and privacy preserving approach to sharing the notifications. 

This is a two step process from the clients perspective:
*   If a user is infected, then the all interactions related to that user should be marked as infected and uploaded from the device to a public location
*   All other devices regularly poll the user for the latest list of infected interaction identifiers and compares that list to the interaction identifiers known locally. If any of them match then the user of the device is notified that they have been in close proximity to someone who is infected and are advised as to further steps to take. 

Note that as every interaction has a unique identifier for each participant, and that as the matching of interactions happens locally there is no correlation analysis possible.

# Server Side Application Architecture

The server application architecture consists of 4 simple, discrete, single-purpose applications hosted in Microsoft Azure. Each is deployed following the least-required-access principle, presenting the minimum of attack surface to clients or other applications, using private networks where appropriate. This is intended to add strength in depth and limit the scope of any attack or other failure in the system.

![What is this element...](Images/Architecture - Applications View.png "Architectural Overview")

## Description

The detection of recent proximity to an infected individual is facilitated by the application downloading Tracing Sets from the Mobile Client Services.

The application workflow starts when a user/patient applies for a test for the currently active pandemic. During the interaction with the care giver, a Tracer is sent from the mobile app containing the Tracer Authorisation Code and the set of recent Tracing Keys. The Tracer Authorisation Code is recorded by the Caregiver in a secure application such as [The application commonly used by Huisarts].

When a positive test result is received by the caregiver, they proceed to the Caregiver Portal to authorise the release of the individual user/patient’s set of Tracing Keys from their Tracer by entering the Tracer Authorisation Code. The portal is protected by a firewall which prevents access unless the browser presents a set of credentials, in this case a certificate registered with the browser, which is authenticated and authorised against the UZI register.

The Caregiver Portal communicates the Tracer Authorisation Code and the Norm Date to the Caregivers Portal Services over a private VLAN. The Tracer Authentication Code is matched with a Tracer which is then marked as authorised for inclusion in the next TracerSet creation job. At this point, any Tracer Key that is not required by being excluded due to the value of the Norm Date is immediately deleted.

The Tracing Sets Engine is responsible for the creation of the Tracing Sets from the Tracers. Once a given Tracer has been included in a Tracing Set, it is deleted. The Tracing Sets Engine is responsible for both Tracer and Tracing set management, which is mostly concerned with the deletion of used or stale data at the earliest possible moment.

We continue to wargame the lifecycle of the data in the above application loop and providing additional functionality to manage edge cases, involving operations staff as a last resort. The system stores both Tracers and Tracing Sets for the minimum time possible time to achieve their role. All data is delete as soon its goal is achieved e.g. Tracers are deleted as soon as they have been added to a Tracing Set or after a set number of days. Tracing Sets are deleted after a set number of days. This design principle and the underpinning DP3T protocol minimise the window of risk for all stored data.

We are currently investigating appropriate mechanisms for encrypting the data both at rest and in motion. This aspect is primarily concerned with the upload and storage of the Tracers and Caregiver Authorisations.


## Technology

The server-side components are all hosted on Microsoft Azure to leverage the security features inherent from that platform.

Mobile Client Services, Caregiver Portal Services are REST APIs deployed as App Services. Caregiver Portal is a web site deployed as an App Service. The Tracing Sets Engine is a set of services hosted as an App Function. 

The live deployment will use Docker and Kubernetes Clusters to enable the system to scale-out/load-balance while achieving redundancy and failover requirements.

All of the above are underpinned by Cosmos Db which provides multiple layers of redundancy to protect the data. 

The applications use the proven .NET Core stack and our own implementation of the DP3T algorithms in Rust.

![What is this element...](Images/Architecture - Overview.png "Architectural Overview")

## Operations

All access tokens are regularly rotated. 

Live system to make the minimum amount of telemetry available to operations to preserve the integrity of the operation of the system.

## Deployment
* Azure - target deployment environment. NL Gov compatible tenant from Microsoft.
* DDoS - Nawasstraat (NL biased traffic filtering)

# References



*   TraceTogether - [ttps://www.gov.sg/article/help-speed-up-contact-tracing-with-tracetogether](https://www.gov.sg/article/help-speed-up-contact-tracing-with-tracetogether)
*   BlueTrace - [www.bluetrace.io](www.bluetrace.io)
*   
*   …

Research Publications



*   Contact Tracing Mobile Apps for COVID-19: Privacy Considerations and Related Trade-offs ([https://arxiv.org/pdf/2003.11511.pdf](https://arxiv.org/pdf/2003.11511.pdf))
*   Quantifying dynamics of SARS-CoV-2 transmission suggests that epidemic control and avoidance is feasible through instantaneous digital contact tracing [https://www.medrxiv.org/content/10.1101/2020.03.08.20032946v1](https://www.medrxiv.org/content/10.1101/2020.03.08.20032946v1)

<!-- Footnotes themselves at the bottom. -->

[^1]:
     [https://www.privacy-regulation.eu/en/article-13-information-to-be-provided-where-personal-data-are-collected-from-the-data-subject-GDPR.htm](https://www.privacy-regulation.eu/en/article-13-information-to-be-provided-where-personal-data-are-collected-from-the-data-subject-GDPR.htm)

[^2]:
     [https://www.privacy-regulation.eu/en/article-5-principles-relating-to-processing-of-personal-data-GDPR.htm](https://www.privacy-regulation.eu/en/article-5-principles-relating-to-processing-of-personal-data-GDPR.htm)

[^3]:
     [https://www.privacy-regulation.eu/en/article-13-information-to-be-provided-where-personal-data-are-collected-from-the-data-subject-GDPR.htm](https://www.privacy-regulation.eu/en/article-13-information-to-be-provided-where-personal-data-are-collected-from-the-data-subject-GDPR.htm)
