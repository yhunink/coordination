# Introduction

The interaction tracing solution provides the framework for interaction detection and the sharing of relevant interaction in a secure and privacy preserving manner. This requires input from the official medical authorities, like the RIVM and GGD, for a number of medically related parameters and the related application logic. As we continue to gain new insights about the virus, it is essential that any solution is able to accommodate the resulting updates in the application parameters and logic in a seamless and timely fashion. The aim of this document is to start documenting these. 

> We are working to get the contents of this document validated by the medical authorities and will integrate their feedback as soon as we can. We welcome other 'COVID-19 related app developers' to make use of this document. Please share your insights so we can use this document as a knowledge repository and limit the load on medical authorities.

# key inputs

The following list provides an overview of key parameters in the interaction tracing solution:

| parameter | description |
| ---| --- |
| normalised infection date | the normalised reference date for an infected person, used to detemrine the relevant period of interactions to be shared. |
| interaction data retention time | the moving time window for which interaction data remain on a user's device (in days). |
| infectious period | the time window for which the interaction data needs to be shared in case of a positive test result, referenced to the normalised. infection date |
| risky interaction | the parameters that define a risky interaction between two people (devices), including (proxies) for distance and time. |
| interaction solution status | A parameter defining whether there is a need for the overall interaction tracing solution to be active and the guidance on the interaction tracing termination process. |

The table provides an overview and description of the key paramters. Next to those, the medical authorities may wish to provide additional COVID-19 related information accessible from the user device. 


