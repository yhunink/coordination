# Introduction
This is a working document intended to capture an overview of how development of PrivateTracer is setup. It is an evolving project, so if you need additional context than below please feel free to reach out to key roles listed below.


# Use cases
There are two core use cases currently detailed out
* Interaction Tracking and Notification
* [Trusted Infection Status](./usecases/TrustedInfectionStatus.md): The interaction between the user, the doctor, the virus testing service and Private Tracer to ensure only validated positive test results are propagated.

# Core components
The following are the core elments of the solution:
* Android Client
* iOS Client
* Cloud hosted services
* Doctor Authorisation of Infection Notification
* DP-3T Protocol: Underlies all the client-services interaction, as well as data stored / generated on device

For additional details on how the components are further broken down on both clients and in the cloud hosted services please consult the Technical Design document.

# Repositories
The following is a high level list of the various projects used by PrivateTracer:
* Coordination: For group level documentation as well as planning boards.
* UX Design: Clickable design for client interfaces, plus related graphical resources
* Server.MobileClientAPI: The set of services that are accessed by client devices
* DP3T-Rust: Implementation of the DP-3T protocol in Rust
* Andriod Client V2: The Android client
* Android DP3T Library: the DP3T protocol wrapper for Android clients
* iOS Client: The iOS client
* iOS DP3T Library: the DP3T wrapper for iOS clients.

# Roles
The following are a subset of the roles on the project, and the reader is asked to recognise that this is both a changing list (both of people and who does what). It is intended to provide a first pass orientation for those newly engaging in the project:
* Initiator & firefigter: Salim Hadri
* Tech Design Focal Point & Delivery Coordinator: Neil Smyth
* Use cases & on-boarding: Rene Honig
* Android Client: Frank Bouwens 
* Proximity Detection: Kaj Oudshoorn
* End-end integration: Michiel Hegemens 
* DP-3T Protocol: Jelle Licht
* Cloud Services: Steve Kellaway
* iOS Client: Dirk Groten
* UX: Mesbah Sabur
* Developer Community: Yvo Hunnink
* Doctor Authorisation: Bart van Riessen
* Privacy & Security Analysis: Aleksander Okonski
* Expert advisor: Dirk-Willem van Gulik

And sincere apologies to the many others that have contributed but whose name I have not put up - felt I had to keep the list short.

# Developer Guidelines
The following are available: 
* [Coding Standards](./Development/CodingStandards.md): Key agreements regarding the code that is checked into the repository.
* ...